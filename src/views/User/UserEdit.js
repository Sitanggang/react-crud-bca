import {
    Card,
    Container,
    Row,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
    Col,
  } from "reactstrap";
  // core components
  import Header from "components/Headers/Header";
  import React, { useState, useEffect } from "react";
  import { Link, useHistory, useParams } from "react-router-dom";
  import axios from "axios";
  
  function UserEdit() {
    let navigate = useHistory();

    const { id } = useParams();
  
    const [user, setUser] = useState({
      name: "",
      username: "",
      email: "",
      password:"",
    });
  
    const { name, username, email, password } = user;
  
    const onInputChange = (e) => {
      setUser({ ...user, [e.target.name]: e.target.value });
    };
  
    useEffect(() => {
      loadUser();
    }, []);
  
    const onSubmit = async (e) => {
      e.preventDefault();
      await axios.put(`http://127.0.0.1:8000/api/user/${id}`, user);
      navigate.push("/admin/user");
    };
  
    const loadUser = async () => {
      const result = await axios.get(`http://127.0.0.1:8000/api/user/${id}`);
      console.log(result.data.data)
      setUser(result.data.data);
    };
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="bg-secondary shadow">
                <CardBody>
                  <Form onSubmit={(e) => onSubmit(e)}>
                    <Row>
                      <Col xs="8 mb-0">
                        <h3 className="heading-small text-muted mb-4">
                          Edit User
                        </h3>
                      </Col>
                      <Col className="text-right" xs="4">
                        <Button color="secondary" tag={Link} to="/admin/user">
                          <div>
                            <i className="fa fa-arrow-left" />
                            <span>Back</span>
                          </div>
                        </Button>
                      </Col>
                    </Row>
                      <div className="pl-lg-4">
                        <FormGroup>
                          <label className="form-control-label">Name</label>
                          <Input
                            type={"text"}
                            className="form-control-alternative"
                            placeholder="Enter your name"
                            name="name"
                            value={name}
                            onChange={(e) => onInputChange(e)}
                            required
                          />
                        </FormGroup>
                        <FormGroup>
                          <label className="form-control-label">Username</label>
                          <Input
                            type={"text"}
                            className="form-control-alternative"
                            placeholder="Enter your username"
                            name="username"
                            value={username}
                            onChange={(e) => onInputChange(e)}
                            required
                          />
                        </FormGroup>
                        <FormGroup>
                          <label className="form-control-label">Email</label>
                          <Input
                            type={"text"}
                            className="form-control-alternative"
                            placeholder="Enter your email"
                            name="email"
                            value={email}
                            onChange={(e) => onInputChange(e)}
                            required
                          />
                        </FormGroup>
                        <FormGroup>
                          <label className="form-control-label">Password</label>
                          <Input
                            type={"password"}
                            className="form-control-alternative"
                            placeholder="Enter your password"
                            name="password"
                            onChange={(e) => onInputChange(e)}
                            required
                          />
                        </FormGroup>
                      </div>
                      <Button
                        type="submit"
                        className="pull-right ml-4"
                        color="primary"
                      >
                        Update
                      </Button>
                  </Form>
                </CardBody>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
  
  export default UserEdit;
  