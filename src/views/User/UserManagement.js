import {
  Card,
  CardHeader,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Alert,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";

// core components
import Header from "components/Headers/Header.js";

const Users = () => {
  const [users, setUsers] = useState([]);
  const [pesan, setPesans] = useState([]);

  const { id } = useParams();

  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const result = await axios.get("http://127.0.0.1:8000/api/user");
    console.log(result.data.user);
    setUsers(result.data.user);
    setPesans(result.data.message);
  };

  const deleteUser = async (id) => {
    await axios.delete(`http://127.0.0.1:8000/api/user/${id}`);
    setModal(!modal);
    loadUsers();
  };

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  const [visible, setVisible] = useState(true);

  return (
    <>
      <Header />
      {/* Page content */}

      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <Col xs="8 mb-0">
                    <h3 className="mb-0">Data Users</h3>
                  </Col>
                  <Col className="text-right" xs="4">
                    <Button color="primary" tag={Link} to="/admin/user/create">
                      Create
                    </Button>
                  </Col>
                </Row>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action </th>
                  </tr>
                </thead>
                <tbody>
                  {users.map((user, index) => (
                    <tr key={user.id}>
                      <th scope="row">{index + 1}</th>
                      <td>{user.name}</td>
                      <td>{user.username}</td>
                      <td>{user.email}</td>
                      <td>
                        <UncontrolledDropdown>
                          <DropdownToggle
                            className="btn-icon-only text-light"
                            href="#pablo"
                            role="button"
                            size="sm"
                            color=""
                            onClick={(e) => e.preventDefault()}
                          >
                            <i className="fas fa-ellipsis-v" />
                          </DropdownToggle>
                          <DropdownMenu className="dropdown-menu-arrow" right>
                            <DropdownItem
                              tag={Link}
                              to={`/admin/user/view/${user.id}`}
                            >
                              Detail
                            </DropdownItem>
                            <DropdownItem
                              tag={Link}
                              to={`/admin/user/edit/${user.id}`}
                            >
                              Edit
                            </DropdownItem>
                            <DropdownItem onClick={toggle}>Delete</DropdownItem>
                            <Modal isOpen={modal} toggle={toggle}>
                              <ModalHeader toggle={toggle}>
                                <h3>Confirmation!!!</h3>
                              </ModalHeader>
                              <ModalBody>
                                Are you sure to delete this user?
                              </ModalBody>
                              <ModalFooter>
                                <Button
                                  color="danger"
                                  onClick={() => deleteUser(user.id)}
                                  className="danger"
                                >
                                  Yes, Delete
                                </Button>{" "}
                                <Button color="secondary" onClick={toggle}>
                                  Cancel
                                </Button>
                              </ModalFooter>
                            </Modal>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <nav aria-label="...">
                  <Pagination
                    className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0"
                  >
                    <PaginationItem className="disabled">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                        tabIndex="-1"
                      >
                        <i className="fas fa-angle-left" />
                        <span className="sr-only">Previous</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem className="active">
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        1
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        2 <span className="sr-only">(current)</span>
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        3
                      </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                      >
                        <i className="fas fa-angle-right" />
                        <span className="sr-only">Next</span>
                      </PaginationLink>
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Users;
