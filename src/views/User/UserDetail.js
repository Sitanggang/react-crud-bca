import {
  Card,
  CardHeader,
  Container,
  Row,
  CardBody,
  Form,
  FormGroup,
  Input,
  Button,
  CardFooter,
  Col,
} from "reactstrap";
// core components
import Header from "components/Headers/Header";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";

function UserDetail() {
  const [user, setUser] = useState({
    name: "",
    username: "",
    email: "",
  });

  const { id } = useParams();

  useEffect(() => {
    loadUser();
  }, []);

  const loadUser = async () => {
    const result = await axios.get(`http://127.0.0.1:8000/api/user/${id}`);
    setUser(result.data.data);
  };
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardBody>
                <Form>
                <Row>
                    <Col xs="8 mb-0">
                      <h5 className="heading-small text-muted mb-4">
                        Detail User
                      </h5>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button color="secondary" tag={Link} to="/admin/user">
                        <div>
                          <i className="fa fa-arrow-left" />
                          <span>Back</span>
                        </div>
                      </Button>
                    </Col>
                  </Row>
                  <div className="pl-lg-4">
                    <FormGroup>
                      <label className="form-control-label">Name</label>
                      <Input
                        disabled
                        className="form-control-alternative"
                        value={user.name}
                      />
                    </FormGroup>
                    <FormGroup>
                      <label className="form-control-label">Username</label>
                      <Input
                        disabled
                        className="form-control-alternative"
                        value={user.username}
                      />
                    </FormGroup>
                    <FormGroup>
                      <label className="form-control-label">Email</label>
                      <Input
                        disabled
                        className="form-control-alternative"
                        value={user.email}
                      />
                    </FormGroup>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
}

export default UserDetail;
